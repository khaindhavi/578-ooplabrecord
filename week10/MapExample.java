import java.util.*;
public class MapExample {
 public static void main(String[] args) {
 Map<String, Integer> studentScores = new HashMap<>();
 studentScores.put("Alice", 90);
 studentScores.put("Bob", 85);
 studentScores.put("Charlie", 95);
 int score1 = studentScores.get("Alice");
 int score2 = studentScores.get("Bob");
 System.out.println("Alice's score: " + score1);
 System.out.println("Bob's score: " + score2);
 boolean containsKey = studentScores.containsKey("Charlie");
 System.out.println("Does map contain Charlie? " + containsKey);
 studentScores.put("Alice", 92);
 studentScores.remove("Bob");
 System.out.println("Keys in the map:");
 for (String key : studentScores.keySet()) {
 System.out.println(key);
 }
 System.out.println("Values in the map:");
 for (int value : studentScores.values()) {
 System.out.println(value);
 }
 System.out.println("Key-value pairs in the map:");
 for (Map.Entry<String, Integer> entry : studentScores.entrySet()) {
 String name = entry.getKey();
 int score = entry.getValue();
 System.out.println(name + ": " + score);
 }
 }
}
