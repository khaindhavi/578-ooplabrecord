
#include<iostream>
using namespace std;
class Evenodd 
{
    public:
    void evenodd(int num) 
    {
        if((num % 2) == 0) 
        cout << num << " is an even number." << endl;
        else
        cout << num << " is an odd number." << endl; 
    }
};
int main()
{
    Evenodd obj;
    int num;
    cout << "Enter a number: ";
    cin >> num; 
    obj.evenodd(num); 
    return 0;
}

