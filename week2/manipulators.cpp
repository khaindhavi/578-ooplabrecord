#include<iostream>
#include<iomanip>
using namespace std;
int main(){
    cout<<"Programming "<<"language"<<endl;//endl – Gives a new line
    cout<<"Programming "<<endl<<"language"<<endl;
    cout <<"only a test"<< flush<<endl;//flush – Flushes the buffer stream
    cout<<setw(10)<<"English"<<setw(5)<<9<<endl;//setw (int n) – To set field width to n 
    cout<<setw(8)<<setfill('$')<<"c++"<<endl;//setfill (Char f) – To set the character to be filled
    float a=4.44444;
    cout<<setprecision(3)<<a<<endl;//setprecision (int p) – The precision is fixed to p 
    istringstream str("     welcome");
    string line;
    getline(str >> std :: ws, line);//ws – Omits the leading white spaces present before the first field
    cout<<line<<endl;
     cout << "hello" << ends << "/,";//ends – Adds null character to close an output string



}