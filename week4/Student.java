import java.util.*;
class Student
{
   String collegeName,name;
   int collegeCode;
   double percentage;
   Student()
   {
    collegeName = "MVGR"; 
    collegeCode = 33;
   }
  Student(String fullName, double semPercentage) {
   
   name = fullName;
   percentage = semPercentage;
  }   
 void display1()
 {
    System.out.println("college name : " +collegeName);
    System.out.println("college code : " +collegeCode); 
 }
 void display2(){

    System.out.println("Full Name : " +name);
    System.out.println("Sem percentage : " +percentage);
 }
 protected void finalize() throws Throwable
    {
       System.out.println("Object destroyed");
    } 


public static void main (String[] args)
 {
     Scanner input = new Scanner(System.in);
     System.out.println("Enter your name: ");
     String fullName = input.nextLine();
     System.out.println("Enter your percentage : ");
     double semPercentage=input.nextDouble();
     Student obj1  = new Student();
     obj1.display1();
     Student obj2 = new Student(fullName,semPercentage);
     obj2.display2();

 }
}
