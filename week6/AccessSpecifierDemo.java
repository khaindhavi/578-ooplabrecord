import java.util.*;
class AccessSpecifierDemo
{
    private int priVar;
    protected int proVar;
    public int pubVar;
    public void setVar(int priValue,int proValue, int pubValue){
        priVar=priValue;
        proVar=proValue;
        pubVar=pubValue;
    }
    public void getVar(){
        System.out.println("Private: "+priVar);
        System.out.println("Protected: "+proVar);
        System.out.println("Public: "+pubVar);
    }
    public static void main(String[]args){
      AccessSpecifierDemo obj=new AccessSpecifierDemo();
      Scanner input = new Scanner(System.in);  
      System.out.println("Enter private value :");
      int pri = input.nextInt();
      System.out.println("Enter public value :");
      int pub = input.nextInt();
      System.out.println("Enter protected value :");
      int pro = input.nextInt();
      obj.setVar(pri,pub,pro);
      obj.getVar();


    }
}