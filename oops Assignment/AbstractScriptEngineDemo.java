import java.io.Reader;

import javax.script.*;

public class AbstractScriptEngineDemo {

    public static void main(String[] args) throws Exception {
        // Create a new instance of the JavaScript engine
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("JavaScript");

        if (engine == null) {
            System.out.println("JavaScript engine not found");
            return;
        }

        // Create a new ScriptContext to use with the engine
        ScriptContext context = new SimpleScriptContext();

        // Set the context for the engine
        engine.setContext(context);

        // Set a variable in the engine scope
        engine.put("x", 10);

        // Evaluate a script using a Reader and Bindings
        String script = "print(x)";
        Bindings bindings = engine.createBindings();
        bindings.put("x", 20);
        Reader reader = new java.io.StringReader(script);
        Object result = engine.eval(reader, bindings);
        System.out.println("Result of script: " + result);

        // Get a value from the engine scope
        Object value = engine.get("x");
        System.out.println("Value of x: " + value);

        // Set a value in the engine scope
        engine.put("y", 30);
        value = engine.get("y");
        System.out.println("Value of y: " + value);

        // Set the Bindings for a specific scope
        engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
        value = engine.eval("x + y");
        System.out.println("Result of x + y: " + value);

        // Get the current ScriptContext for the engine
        ScriptContext currentContext = engine.getContext();
        System.out.println("Current ScriptContext: " + currentContext);
    }
}

