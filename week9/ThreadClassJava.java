public class ThreadClassJava 
{
 public static void main(String[] args)
 {
   MyThread1 obj = new MyThread1();
   obj.start();     //start() method is built-in method in thread class
 }

}
class MyThread1 extends Thread   // MyThread1 class extends the properties of Thread class
{
    public void run()
    {
        for(int i=0; i<5; i++)
        System.out.println(i);
    }
}
