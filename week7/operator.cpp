
//Program to demonstrate subtraction overloading
#include <iostream>
using namespace std;
class Complex {
   private:
    float real;
    float imag;

   public:
    void input() {
        cout << "Enter real and imaginary parts respectively: ";
        cin >> real;
        cin >> imag;
    }

    // Overload the - operator
    Complex operator - (const Complex& obj) {
        Complex obj1;
        obj1.real = real - obj.real;
        obj1.imag = imag - obj.imag;
        return obj1;
    }

    void output() {
        if (imag >= 0)
            cout << "Output Complex number: " << real << " + i "<<imag <<endl;
        else
            cout << "Output Complex number: " << real << " + i (" << imag << ")"<<endl;
    }
};

int main() {
    Complex complex1, complex2, result;

    cout << "Enter first complex number:\n";
    complex1.input();

    cout << "Enter second complex number:\n";
    complex2.input();

    result = complex1 - complex2;
    result.output();

}
