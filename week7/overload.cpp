//C++ program to demonstrate overloading of methods
#include<iostream>
using namespace std;
class M_Overload
{
 public:
 string fullName,deptName,subjName;
 int exp;
 double salaryPackage;
 void teacher(string name,string dname,string sname) 
 {
   fullName = name;
   deptName = dname;
   subjName = sname;
 }
 void teacher(int years) 
 {
   exp = years;
 }
 void teacher(double spackage)
 {
   salaryPackage = spackage;
 }
 void display()
 {
     cout << "Full Name: " << fullName << endl;
     cout << "Department Name:" << deptName << endl;
     cout << "Subject name: " << subjName << endl;
     cout << "Years of experience: " << exp << endl;
     cout << "Salary Package: " << salaryPackage << endl;

 }
};
int main()
{
    string name,dname,sname;
    int years;
    double spackage;
    cout << "Enter the name: " << endl;
    cin >> name;
    cout << "Enter department name: " << endl;
    cin >> dname;
    cout << "Enter subject name: " << endl;
    cin >> sname;
    cout << "Enter years of experience: " << endl;
    cin >> years;
    cout << "Enter the salary package in LPA: "<< endl;
    cin >> spackage;
    M_Overload obj; 
    obj.teacher(name,dname,sname); 
    obj.teacher(years); 
    obj.teacher(spackage); 
    obj.display();  

}
