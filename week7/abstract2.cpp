
#include <iostream>
using namespace std;
class Science
{
  public:
    void study();
    void category()
    {
        cout << "Biological Science" << endl;
    }
};
class Botony : public Science
{
  public:
  void study() 
  {
    cout << "Botony is study of plants" << endl;
  }
};
class Zoology : public Science
{
  public:
  void study() 
  {
    cout << "Zoology is study of animals" << endl;
  }
};
int main()
{
 Botony b;
 Zoology z;
 b.category();
 b.study();
 z.category();
 z.study();
}
