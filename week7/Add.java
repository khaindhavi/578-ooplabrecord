public class Add{
    public int add(int a, int b){
        return a+b;
    }
    public float add(float a, float b, float c){
        return a+b+c;
    }

    public static void main(String[] args) {
        Add function = new Add();
        System.out.println(function.add(2,3));
        System.out.println(function.add(2,6,4));
        System.out.println(function.add(5,6));    
    }
}
