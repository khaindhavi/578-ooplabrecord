import java.util.*;
abstract class Animal 
{
  abstract void sound(); 
  void action()
  {
    System.out.println("Shouting");
  }
}
class Dog extends Animal
{
  void sound()
  {
    System.out.println("Bow-Bow");
  }
}
class Cat extends Animal
{
  void sound()
  {
    System.out.println("Meow-Meow");
  }
}
public class ParAbsJava
{
    public static void main(String args[])
    { 
    Dog d = new Dog(); 
    Cat c= new Cat(); 
    d.action(); 
    d.sound(); 
    c.action(); 
    c.sound(); 
    }   
}

