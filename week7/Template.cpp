#include<iostream>
using namespace std;
int result=0;

class Template{
    public:
    template <typename myDataType1>
    void add(myDataType1 a, myDataType1 b){
        myDataType1 result;
        result=a+b;
        cout<<result<<endl;
    }
};

int main(){
    Template temp;
    temp.add<int>(2.1,3.1);
    temp.add<float>(2.1,3.1);
}
