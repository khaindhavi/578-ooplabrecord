
import java.util.*;
class Base          //Base class
{
    int add(int n1,int n2,int n3) // function definition of add() with three parameters
    {
        return n1+n2+n3;
    }
}
class Derived extends Base  //Derived class is inheriting the properties of Base class
{
    int add(int n1, int n2) // function definition of add() with two parameters
    {
        return n1+n2;
    }
}
public class MethodOLInheriJava 
{
    public static void main (String[] args)
    {
    Derived obj = new Derived();
    Scanner input = new Scanner(System.in); 
    System.out.println("Enter the values : ");
    int num1 = input.nextInt();
    int num2 = input.nextInt(); 
    int num3 = input.nextInt();
    System.out.println("Addition of two numbers : " +(obj.add(num1,num2))); 
    System.out.println("Addition of three numbers : " +(obj.add(num1,num2,num3)));
    }
}

