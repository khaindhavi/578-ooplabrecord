import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class JobPortalPbl extends JFrame {
   private JPanel studentPanel, companyPanel, jobPanel, profilePanel, applyPanel, dashboardPanel;
   private JTabbedPane tabbedPane;
   private JLabel studentNameLabel, studentEmailLabel, studentPasswordLabel, companyNameLabel, companyEmailLabel, companyPasswordLabel, jobLabel, locationLabel, salaryLabel, jobTypeLabel;
   private JTextField studentNameField, studentEmailField, companyNameField, companyEmailField, jobField, locationField, salaryField;
   private JPasswordField studentPasswordField, companyPasswordField;
   private JButton studentLoginButton, studentRegisterButton, companyLoginButton, companyRegisterButton, jobButton, applyButton, profileButton, dashboardButton;
   private JTextArea jobTextArea, applyTextArea;
   private JComboBox jobTypeComboBox;

   private List<Job> jobs;
    private List<Student> students;
    private List<Company> companies;

   public JobPortalPbl() {
      setTitle("Online Job Portal");
      setSize(800, 600);

      jobs = new ArrayList<>();
      students = new ArrayList<>();
      companies = new ArrayList<>();

      // create the student panel
      studentPanel = new JPanel();
      studentPanel.setBackground(Color.WHITE);
      studentPanel.setLayout(new FlowLayout(FlowLayout.CENTER,20,300) );
      studentNameLabel = new JLabel("UserName:");
      studentPanel.add(studentNameLabel);
      studentNameField = new JTextField(10);
      studentPanel.add(studentNameField);
      studentEmailLabel = new JLabel("Email:");
      studentPanel.add(studentEmailLabel);
      studentEmailField = new JTextField(10);
      studentPanel.add(studentEmailField);
      studentPasswordLabel = new JLabel("Password:");
      studentPanel.add(studentPasswordLabel);
      studentPasswordField = new JPasswordField(10);
      studentPanel.add(studentPasswordField);
      studentLoginButton = new JButton("Login");
      studentPanel.add(studentLoginButton);
      studentRegisterButton = new JButton("Register");
      studentPanel.add(studentRegisterButton);

      // create the company panel
      companyPanel = new JPanel();
      companyPanel.setLayout(new FlowLayout(FlowLayout.CENTER,20,300) );
      companyNameLabel = new JLabel("UserName:");
      companyPanel.add(companyNameLabel);
      companyNameField = new JTextField(10);
      companyPanel.add(companyNameField);
      companyEmailLabel = new JLabel("Email:");
      companyPanel.add(companyEmailLabel);
      companyEmailField = new JTextField(10);
      companyPanel.add(companyEmailField);
      companyPasswordLabel = new JLabel("Password:");
      companyPanel.add(companyPasswordLabel);
      companyPasswordField = new JPasswordField(10);
      companyPanel.add(companyPasswordField);
      companyLoginButton = new JButton("Login");
      companyPanel.add(companyLoginButton);
      companyRegisterButton = new JButton("Register");
      companyPanel.add(companyRegisterButton);

      // create the job panel
      jobPanel = new JPanel();
      jobPanel.setLayout(new FlowLayout(FlowLayout.CENTER,20,100));
      jobLabel = new JLabel("Job Title:");
      jobPanel.add(jobLabel);
      jobField = new JTextField(10);
      jobPanel.add(jobField);
      
      JLabel JlocationLabel = new JLabel("Location:");
      jobPanel.add(JlocationLabel);
      JTextField JlocationField = new JTextField(10);
      jobPanel.add(JlocationField);
      JLabel JsalaryLabel = new JLabel("Salary:");
      jobPanel.add(JsalaryLabel);
      JTextField JsalaryField = new JTextField(10);
      jobPanel.add(JsalaryField);
      jobTypeLabel = new JLabel("Job Type:");
      jobPanel.add(jobTypeLabel);
      String[] jobTypes = {"Full-time", "Part-time", "Internship"};
      jobTypeComboBox = new JComboBox(jobTypes);
      jobPanel.add(jobTypeComboBox);
      jobButton = new JButton("Add Job");
      jobPanel.add(jobButton);

      jobTextArea = new JTextArea(20, 50);
      JScrollPane jobScrollPane = new JScrollPane(jobTextArea);
      jobPanel.add(jobScrollPane);

       // create the apply panel
       applyPanel = new JPanel();
       applyPanel.setLayout(new FlowLayout(FlowLayout.CENTER,20,150));
       JLabel applyLabel = new JLabel("Job Title:");
       applyPanel.add(applyLabel);
       JTextField applyField = new JTextField(20);
       applyPanel.add(applyField);
       
       locationLabel = new JLabel("Location:");
       applyPanel.add(locationLabel);
       locationField = new JTextField(10);
       applyPanel.add(locationField);
       salaryLabel = new JLabel("Expected Salary per month:");
       applyPanel.add(salaryLabel);
       salaryField = new JTextField(10);
       applyPanel.add(salaryField);
       JLabel applyTypeLabel = new JLabel("Job Type:");
       applyPanel.add(applyTypeLabel);
       String[] applyjobTypes = {"Full-time", "Part-time", "Internship"};
       jobTypeComboBox = new JComboBox(applyjobTypes);
       applyPanel.add(jobTypeComboBox);
       applyButton = new JButton("Apply Job");
       applyPanel.add(applyButton);
       JButton companyListButton = new JButton("Companies list");
       applyPanel.add(companyListButton);
       JTextArea applyjobTextArea = new JTextArea(20, 50);
       JTextArea listjobTextArea = new JTextArea(10, 20);
       JScrollPane applyjobScrollPane = new JScrollPane(applyjobTextArea);
       applyPanel.add(applyjobScrollPane);
      // create the tabbed pane and add the panels
      tabbedPane = new JTabbedPane();
      tabbedPane.addTab("Students", studentPanel);
      tabbedPane.addTab("Companies", companyPanel);
      tabbedPane.addTab("PostJobs", jobPanel);
      tabbedPane.addTab("ApplyJob", applyPanel);
      add(tabbedPane);

      
      //student login
      studentLoginButton.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            String username = studentNameField.getText();
            String password = new String(studentPasswordField.getPassword());
           for (Student student : students) {
            if (student.getUsername().equals(username) && student.getPassword().equals(password)) {
             JOptionPane.showMessageDialog(studentPanel, "login Successful");
         }
         else{
            JOptionPane.showMessageDialog(studentPanel, "invalid");

         }
      }

      
   }
     });

     studentRegisterButton.addActionListener(new ActionListener() {
      @Override
        public void actionPerformed(ActionEvent e) {
           // student registration logic
           String username = JOptionPane.showInputDialog(this, "Enter username:");
        if (username == null) {
            return;
        }
        for (Student student : students) {
         if (student.getUsername().equals(username)) {
             JOptionPane.showMessageDialog(null, "Username already exists.", "Error", JOptionPane.ERROR_MESSAGE);
             return;
         }
     }
   
     String password = JOptionPane.showInputDialog(this, "Enter password:");
     if (password == null) {
         return;
     }
     students.add(new Student(username, password));
     String dob=JOptionPane.showInputDialog(this,"Enter date of birth");
     String[] genderOptions = {"Male", "Female", "Other"};
     JComboBox studentGenderComboBox = new JComboBox<>(genderOptions);
     String address=JOptionPane.showInputDialog(this,"Address");
     String skill=JOptionPane.showInputDialog(this,"skill");
     JOptionPane.showMessageDialog(studentPanel, "Registration Successfull");


           
        }
     });

     companyLoginButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
         // company login logic
      String username =companyNameField.getText();
      String password = new String(companyPasswordField.getPassword());
      for (Company company : companies) {
         if (company.getUsername().equals(username) && company.getPassword().equals(password)) {
            JOptionPane.showMessageDialog(companyPanel, "login Successful");
         }
         else{
            JOptionPane.showMessageDialog(companyPanel, "Invalid email or password.Please Register");

         }
      }

      
   }
              
        
   });

   companyRegisterButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
         // company registration logic
         String username = JOptionPane.showInputDialog(this, "Enter username:");
        if (username == null) {
            return;
        }
        for (Company company : companies) {
         if (company.getUsername().equals(username)) {
             JOptionPane.showMessageDialog(null, "Username already exists.", "Error", JOptionPane.ERROR_MESSAGE);
             return;
         }
     }
        
        String password = JOptionPane.showInputDialog(this, "Enter password:");
        if (password == null) {
            return;
        }
        companies.add(new Company(username, password));
        String cname = JOptionPane.showInputDialog(this, "Company name:");
        String ccity = JOptionPane.showInputDialog(this, "City:");
        String cemail = JOptionPane.showInputDialog(this, "Email:");
        String cno=JOptionPane.showInputDialog(this, "Contact number:");
        JOptionPane.showMessageDialog(null,"Registration successfull.");

      }
   });





      jobButton.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
           // add job logic
           String jobTitle = jobField.getText();
           String jobLocation = JlocationField.getText();
           String jobSalary = JsalaryField.getText();
           String jobType = jobTypeComboBox.getSelectedItem().toString();
           jobTextArea.append(jobTitle + " (" + jobLocation + ") " +jobType  + " - $" + jobSalary + "/year\n");
           jobField.setText("");
           JlocationField.setText("");
           JsalaryField.setText("");
        }
     });

     applyButton.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
           // apply job logic
           String appliedjobTitle = applyField.getText();
           String appliedjobLocation = locationField.getText();
           String appliedjobSalary = salaryField.getText();
           String applyjobType = jobTypeComboBox.getSelectedItem().toString();
           applyjobTextArea.append(appliedjobTitle + " (" + appliedjobLocation + ") " + applyjobType + " - $" + appliedjobSalary + "/year\n");
           applyField.setText("");
           locationField.setText("");
           salaryField.setText("");
      }
     });

     companyListButton.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
         String appliedjobTitle = applyField.getText();
            listjobTextArea.append(appliedjobTitle+"TCS\nWipro\nInfosys\nTech Mahindra\nAccenture\nOracle\nCapgemini\nAmazon\nGoogle\nDeloitte");
        }
    });





      // set up the frame
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setVisible(true);
   }
   private void tGray() {
   }
   public static void main(String[] args) {
    JobPortalPbl jobPortal = new JobPortalPbl();
    jobPortal.setVisible(true);
    }
    }
    class User {
      private String username;
      private String password;
      public User(String username, String password) {
          this.username = username;
          this.password = password;
      }
      
      public String getUsername() {
          return username;
      }
      
      public String getPassword() {
          return password;
      }
      }
      
      class Student extends User {
      public Student(String username, String password) {
      super(username, password);
      }
      }
      
      class Company extends User {
      public Company(String username, String password) {
      super(username, password);
      }
      }