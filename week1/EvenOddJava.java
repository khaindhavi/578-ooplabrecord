
import java.util.*;
class EvenOddJava
{
    void EvenOdd(int num)
    {
        if(num % 2 == 0)
        System.out.println(num + " is an even number."); 
        else
        System.out.println(num + " is an odd number."); 
    }
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in); 
        System.out.println("Enter a number: ");
        int num = input.nextInt();
        EvenOddJava obj = new EvenOddJava();
        obj.EvenOdd(num);
    }
}



