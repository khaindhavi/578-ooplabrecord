#include <iostream>

using namespace std;

class Animal {
public:
    void eat() { cout << "Animal::eat()" << endl; }
};

class Mammal : virtual public Animal { // virtual inheritance
public:
    void breathe() { cout << "Mammal::breathe()" << endl; }
};

class Bird : virtual public Animal { // virtual inheritance
public:
    void fly() { cout << "Bird::fly()" << endl; }
};

class Bat : public Mammal, public Bird {
public:
    void feedBaby() { cout << "Bat::feedBaby()" << endl; }
};

int main() {
    Bat b;
    b.eat(); // no ambiguity error
    b.breathe();
    b.fly();
    b.feedBaby();

    return 0;
}


