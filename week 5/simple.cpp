#include <iostream>
using namespace std;

// Base class
class Person {
  public:
    string name;
    int age;

    void introduce(){
        cout << "Hi, my name is " << name << " and I am " << age << " years old." << endl;
    }
};

// Derived class
class Student : public Person {
  public:
    string major;

    void study(){
        cout << name << " is studying " << major << "." << endl;
    }
};

int main() {
    // create an instance of the Student class
    Student havi;
    havi.name = "Havi";
    havi.age = 20;
    havi.major = "Computer Science";

    // call methods from the base and derived class
    havi.introduce();
    havi.study();

    return 0;
}



