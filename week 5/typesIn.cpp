#include <iostream>
using namespace std;

class Greetings {
   

   private:
    void display2() {
        cout << "Hi" << endl;
    }

   protected:
    void display3() {
        cout << "Namasthey" << endl;
    }
    public:
    void display1() {
        cout << "Hello" << endl;
    }
    void getprivate(){
        display2();
    }
};


class WishesPrivate: private Greetings {
   public:
    void greet() {
        cout<<"private inheritence:"<<endl;
        display1();
        display3();

    }
};

class WishesPublic : public Greetings {
   public:
    void greet() {
        cout<<"public inheritence:"<<endl;
        display1();
        display3();
    }
};


class WishesProtected : protected Greetings {
   public:
    void greet() {
        cout<<"protected inheritence:"<<endl;
        display1();
        display3();

    }
};

int main() {
    WishesPrivate obj1;
    obj1.greet();
    WishesPublic obj2;
    obj2.greet();
    obj2.getprivate();
    WishesProtected obj3;
    obj3.greet();

}