class Greetings {

    // field and method of the parent class
    String name;
    public void greet() {
      System.out.println("Namasthey");
    }
  }
  
  // inherit from Animal
  class Intro extends Greetings {
  
    // new method in subclass
    public void display() {
      System.out.println("My name is " + name);
    }
  }
  
  class Main {
    public static void main(String[] args) {
  
      // create an object of the subclass
      Intro details = new Intro();

      // call method of superclass
      // using object of subclass
      details.greet();
  
      // access field of superclass
      details.name = "Rohu";
      details.display();
  
    }
  }
 
 