#include "iostream"
#include "boxArea.h"
#include "boxVolume.h"
using namespace std;
int main(){
    float h,w,l;
    cout << "Enter the length, height and width of the box: " ;
    cin >> l >> h >> w;
    boxArea(l,w,h);
    boxVolume(l,w,h);
}
