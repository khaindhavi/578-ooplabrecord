class MutablePerson {
    private String name;
    
    public MutablePerson(String name) {
        this.name = name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
}


final class ImmutablePerson {
    private final String name;
    
    public ImmutablePerson(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
}

public class Mute {
    public static void main(String[] args) {

        // Demonstrating a mutable object
        MutablePerson mutablePerson = new MutablePerson("John");
        System.out.println("Mutable Person's name: " + mutablePerson.getName());
        mutablePerson.setName("Jane");
        System.out.println("Mutable Person's name after mutation: " + mutablePerson.getName());
        
        // Demonstrating an immutable object
        ImmutablePerson immutablePerson = new ImmutablePerson("Jack");
        System.out.println("Immutable Person's name: " + immutablePerson.getName());
        
        // This will not compile because the name field is final and cannot be changed
        // immutablePerson.setName("Jill");
    }
}

